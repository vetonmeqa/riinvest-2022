import constants from "./constants";

const initialState = { posts: [] };

export default function posts(state = initialState, action) {
  switch(action.type) {
    case constants.GET: 
      return {
        posts: action.posts, 
      };
    case constants.CLEAR: 
      return initialState;
    default: 
      return state;
  }
}