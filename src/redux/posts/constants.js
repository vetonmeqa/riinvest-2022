const constants = {
  GET: 'POSTS_GET',

  CLEAR: 'POSTS_CLEAR'
};

export default constants;