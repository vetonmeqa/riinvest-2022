import constants from "./constants";
import DATA from "./DATA";

export const getPosts = async () => {
  const response = await fetch('https://reactnative.dev/movies.json');
  const responseJson = await response.json();
  return {
    type: constants.GET,
    posts: responseJson.movies
  }
}

export const clear = () => {
  return {
    type: constants.CLEAR,
  }
}