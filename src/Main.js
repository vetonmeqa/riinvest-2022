import React from 'react';
import { SafeAreaView, View, StyleSheet } from 'react-native';
import AppStack from './AppStack';
import ThemeContextProvider from './context/ThemeContextProvider';

const Main = () => {
  return (
    <SafeAreaView style={styles.outerContainer}>
      
      <View style={styles.container}>

        <ThemeContextProvider>

          <AppStack />

        </ThemeContextProvider>

      </View>

    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },
  container: {
    flexGrow: 1,
    backgroundColor: "#fff"
  }
});

export default Main;