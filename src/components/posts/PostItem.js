import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const PostItem = ({ post }) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image source={{uri: post.imageUrl }} style={styles.profileImage} />
        <Text style={styles.username}>
          {post.userName}
        </Text>
      </View>

      <Image source={{uri: post.imageUrl }} style={styles.image} />
    </View>
  )
}

const styles = StyleSheet.create({ 
  container: {
    height: 250
  },
  header: {
    flexDirection: 'row',
    margin: 10
  },  
  profileImage: {
    height: 30,
    width: 30,
    borderRadius: 50,
    marginRight: 5
  },
  username: {
    fontWeight: '600'
  },
  image: {
    height: 200, 
    width: '100%'
  }
});

export default PostItem;