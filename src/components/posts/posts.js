import React, { useEffect, useContext } from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import ThemeContext from '../../context/ThemeContext';
import PostItem from './PostItem';

//redux imports
import { useDispatch, useSelector } from 'react-redux';
import { getPosts } from '../../redux/posts/actions';

const Posts = () => {
  const dispatch = useDispatch();
  const themeContext = useContext(ThemeContext);
  const { theme } = themeContext;
  const { posts } = useSelector(({ posts }) => posts);

  // trigger action to get data on component first load
  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    // get data through redux
    dispatch(getPosts());
    
    // getPostsApiToComponent();
  }

  // const getPostsApiToComponent = async () => {
  //   // get data direct to component
  //   try {
  //     const response = await fetch('https://reactnative.dev/movies.json');
  //     const posts = await response.json();
  //     setPosts(posts.movies);
  //   } catch (e) {
  //     console.log(e);
  //   }
  // }

  return (
    <View style={styles.container}>
      
      <FlatList 
        data={posts} 
        renderItem={({ item }) => <PostItem post={item}/>}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10, 
    flexDirection: 'row'
  }
});

export default Posts;