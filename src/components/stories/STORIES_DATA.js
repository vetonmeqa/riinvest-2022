const STORIES_DATA = [
  {
    id: 123123,
    userName: 'Adi Sylejmani',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
  {
    id: 1251233,
    userName: 'Albin Bllaca',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
  {
    id: 121231254,
    userName: 'Anesa Gorani',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
  {
    id: 1231231,
    userName: 'Besfort Vitija',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
  {
    id: 1231241,
    userName: 'Anesa Gorani',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
  {
    id: 123412,
    userName: 'Besfort Vitija',
    imageUrl: 'https://reactnative.dev/img/homepage/phones.png',
    seen: false
  },
];

export default STORIES_DATA;
