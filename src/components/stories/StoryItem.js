import React from 'react';
import { TouchableOpacity, Image, Text, StyleSheet } from 'react-native';

const StoryItem = ({ story, onOpen }) => {
  return (
    <TouchableOpacity style={styles.outerContainer} onPress={() => onOpen(story)}>
  
      <Image source={{ uri: story.imageUrl }} style={{
          ...styles.innerContainer, ...(!story.seen && {...styles.notSeenContainer})
        }}
      />
   
      <Text style={styles.text}>{story.userName}</Text>
     
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  outerContainer: {
    margin: 10,
  },
  innerContainer: {
    alignSelf: 'center',
    backgroundColor: 'red',
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 50,
    height: 60,
    width: 60,
    marginBottom: 10,
  },
  notSeenContainer: {
    borderWidth: 2,
    borderColor: 'red'
  },
  text: {
    textAlign: 'center'
  }
});

export default StoryItem;