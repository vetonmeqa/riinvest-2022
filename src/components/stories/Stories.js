import React, {useState} from 'react';
import { View, StyleSheet, FlatList } from 'react-native';
import STORIES_DATA from './STORIES_DATA';
import StoryItem from './StoryItem';
import { useNavigation } from '@react-navigation/native';

const Stories = () => {
  const navigation = useNavigation();

  const [stories, setStories] = useState(STORIES_DATA);

  const onItemOpen = (item) => {
    const nextStoriesState = stories.map(story => { return story.id === item.id ? {...story, seen: true} : story });
    setStories(nextStoriesState);
    navigation.navigate("Detail");
  }

  return (
    <View style={styles.container}>

      <FlatList 
        horizontal={true}
        data={stories}
        renderItem={({ item }) => <StoryItem story={item} onOpen={onItemOpen} />}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10, 
    flexDirection: 'row'
  }
});

export default Stories;
