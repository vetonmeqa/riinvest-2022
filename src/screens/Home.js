import React from 'react';
import Stories from '../components/stories/Stories';
import Posts from '../components/posts/Posts';

const Home = () => {
  return (
      <>

        <Stories />
        <Posts />

      </>
  );
}

export default Home;