import React from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './screens/Home';
import DetailScreen from './screens/Detail';

// Stack Navigation
const Stack = createStackNavigator();

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={HomeScreen} />
      <Stack.Screen name="Detail" component={DetailScreen} />
    </Stack.Navigator>
  );
}

// Default Screen component
function DefaultScreen() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Default!</Text>
    </View>
  );
}

// Bottom Navigation
const Tab = createBottomTabNavigator();

function BottomTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={HomeStack} />
      <Tab.Screen name="Search" component={DefaultScreen} />
      <Tab.Screen name="Create" component={DefaultScreen} />
      <Tab.Screen name="Notifications" component={DefaultScreen} />
      <Tab.Screen name="Account" component={DefaultScreen} />
    </Tab.Navigator>
  );
}

function AppStack() {
  return (
    <NavigationContainer>
      <BottomTabs />
    </NavigationContainer>
  );
}


export default AppStack;