import { createContext } from 'react';

var ThemeContext = createContext();

export default ThemeContext;